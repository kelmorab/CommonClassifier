#include "TTH/CommonClassifier/interface/MEMClassifier.h"
#include "TFile.h"
#include <iostream>

using namespace std;
using namespace MEM;

const TLorentzVector p4(double pt, double eta, double phi, double mass) {
    TLorentzVector lv;
    lv.SetPtEtaPhiM(pt, eta, phi, mass);
    return lv;
}

int main(){

  //Setup the MEM
  MEMClassifier mem;

  //Add some objects to the MEM
  std::vector<TLorentzVector> jets_p4_nominal = {
      p4(
        51.62411880493164, 
        -0.8125799298286438, 
        -3.0860791206359863, 
        7.721531867980957
      ), 
      p4(
        51.45940399169922, 
        -2.1350724697113037, 
        2.794322967529297, 
        6.909081935882568
      ), 
      p4(
        38.61643981933594, 
        -1.6606312990188599, 
        0.2091505527496338, 
        5.794722080230713
      ), 
      p4(
        22.914491653442383, 
        -0.4653875529766083, 
        -0.5745444893836975, 
        3.8986964225769043
      )
  };
  
  auto leps_p4 = {
    p4(
        63.742523193359375,
        -1.606768250465393,
        -0.6526610851287842,
        0.10570000112056732
    ),
    p4(
        30.206972122192383,
        -1.7477864027023315,
        1.7761988639831543,
        0.015261447988450527
    )
  };
  
  vector<double> variations = {0.9, 1.0, 1.1};
  vector<vector<double>> jet_variations;
  for (unsigned int ijet=0; ijet<jets_p4_nominal.size(); ijet++) {
    jet_variations.push_back(variations);
  }

std::vector<std::vector<TLorentzVector> > jets_p4;
jets_p4.push_back(jets_p4_nominal);
for (unsigned int ijet=0; ijet<jets_p4_nominal.size(); ijet++) {
  std::vector<TLorentzVector> p4_var;
  for (unsigned int i = 0; i<variations.size(); i++) {
    TLorentzVector lv;
    lv.SetPtEtaPhiM(jets_p4_nominal[ijet].Pt()*jet_variations[ijet][i],jets_p4_nominal[ijet].Eta(),jets_p4_nominal[ijet].Phi(),jets_p4_nominal[ijet].M()*jet_variations[ijet][i]);
    p4_var.push_back(lv);
  }
  jets_p4.push_back(p4_var);
}

//Assume for now that it doesn't modify the event category
vector <bool> changes_jet_category;
for (int i=0; abs(i)<abs(jet_variations[0].size()) + 1; i++){
  changes_jet_category.push_back(false);
}

std::vector<std::vector<double> > jets_csv;
std::vector<std::vector<MEMClassifier::JetType> > jets_type;
std::vector<std::vector<std::vector<double> > > jets_variations;
for (unsigned int i = 0; i<variations.size()+1; i++) {
jets_csv.push_back({
      0.929882630706,
      0.990463197231,
      0.899696052074,
      0.979629218578,
    });
jets_type.push_back({
      MEMClassifier::JetType::RESOLVED,
      MEMClassifier::JetType::RESOLVED,
      MEMClassifier::JetType::RESOLVED,
      MEMClassifier::JetType::RESOLVED,
    });
jets_variations.push_back(jet_variations);
}

  //create a MET
  TLorentzVector lv_met;
  lv_met.SetPtEtaPhiM(
      45.685523986816406,
      0.0,
      0.9771100282669067,
      0.0
  );
  auto res = mem.GetOutput(
    leps_p4,
    {-1, 1},
    jets_p4,
    jets_csv,
    jets_type,
    jets_variations,
    lv_met,
    changes_jet_category
  );

  std::cout << "mem=" << res[0].p << std::endl;
  for (unsigned int ivar=0; ivar < res[0].p_variated.size(); ivar++) {
    if (changes_jet_category[ivar+1] == false) {
      std::cout << "mem var " << ivar << " = " << res[0].p_variated.at(ivar) << std::endl;
    }
    else std::cout << "mem var " << ivar << " = " << res[ivar].p_variated.at(ivar) << std::endl;
  }
}
